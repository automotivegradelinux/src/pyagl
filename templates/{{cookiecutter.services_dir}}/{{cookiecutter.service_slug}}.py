# Copyright (C) 2020 Konsulko Group
# Author:
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from pyagl.services.base import AGLBaseService, AFBResponse
import asyncio
import os


class {{cookiecutter.classname}}(AGLBaseService):
    service = '{{cookiecutter.aglsystemdservice}}'
    parser = AGLBaseService.getparser()

    def __init__(self, ip, port=None, service='{{cookiecutter.aglsystemdservice}}'):
        super().__init__(api='{{cookiecutter.api}}', ip=ip, port=port, service=service)
        # more init stuff specific to the new service

async def main(loop):
    args = {{cookiecutter.classname}}.parser.parse_args()
    svc = await {{cookiecutter.classname}}(args.ipaddr)

