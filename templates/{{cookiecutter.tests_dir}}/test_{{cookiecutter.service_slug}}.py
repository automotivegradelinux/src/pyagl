# Copyright (C) 2020 Konsulko Group
# Author:
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import asyncio
import os
import pytest
import logging
from pyagl.services.base import AFBResponse, AFBT
from concurrent.futures import TimeoutError

from pyagl.services.{{cookiecutter.service_slug}} import {{cookiecutter.classname}}
pytestmark = pytest.mark.asyncio

@pytest.fixture(scope='module')
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop

@pytest.fixture(scope='module')
async def service():
    address = os.environ.get('AGL_TGT_IP', 'localhost')
    port = os.environ.get('AGL_TGT_PORT', None)
    svc = await {{cookiecutter.classname}}(ip=address, port=port)
    yield svc

