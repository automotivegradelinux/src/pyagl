# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pyagl",
    version="0.0.1",
    author="Edi Feschiyan",
    author_email="edi.feschiyan@konsulko.com",
    description="Python bindings and tests for Automotive Grade Linux services",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/refresher/pyagl",
    packages=setuptools.find_packages(),
    include_package_data=True,
    license="Apache 2.0",
    install_requires=['websockets', 'parse', 'asyncssh', 'pytest', 'pytest-asyncio', 'pytest-dependency'],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: POSIX :: Linux",
    ],
    python_requires='>=3.6',
)
