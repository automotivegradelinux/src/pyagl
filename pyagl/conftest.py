# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import re
import asyncio
import pytest
import argparse
import time
import asyncssh
import subprocess


class LavaAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs=0, **kwargs):
        if nargs !=  0:
            raise ValueError("nargs not allowed")
        super(LavaAction, self).__init__(option_strings, dest, nargs=nargs, **kwargs)
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, True)
        setattr(namespace, 'color', 'no')


def pytest_addoption(parser):
    parser.addoption('-L', '--lava', action=LavaAction, help='enable LAVA signals')

    # Hook to allow not skipping signal-composer tests for debugging
    parser.addoption('--run-signal-composer', action='store_true', default=False, help='run signal-composer tests')


def pytest_configure(config):
    # Force normal progress and verbose output off when doing LAVA output
    terminal = config.pluginmanager.getplugin('terminal')
    class QuietReporter(terminal.TerminalReporter):
        def _determine_show_progress_info(self):
            return False

        @property
        def verbosity(self):
            return 0

        @property
        def showlongtestinfo(self):
            return False

        @property
        def showfspath(self):
            return False

    if config.getoption('lava'):
        terminal.TerminalReporter = QuietReporter


def lava_result_convert(pytest_outcome):
    """ Convert the pytest outcome to the string expected by LAVA."""
    if pytest_outcome == 'passed':
        return 'pass'
    elif pytest_outcome == 'skipped':
        return 'skip'
    elif pytest_outcome == 'xfailed':
        return 'pass'
    else:
        return 'fail'


def pytest_report_teststatus(config, report):
    """ Insert strings that LAVA expects to capture test results."""
    done = False
    if config.getoption('lava'):
        # Convert pytest test file and name into a LAVA test name
        test_file = report.location[0].split('/')[-1]
        test_file = test_file.replace('test_', '', 1)
        if test_file.endswith('.py'):
            test_file = test_file[:-3]
        test_name = test_file + '_' + report.location[2][5:]
        test_result = lava_result_convert(report.outcome)

        # Generate expected LAVA testcase output
        if report.when == 'setup':
            if report.outcome == 'skipped':
                done = True
        elif report.when == 'call':
            done = True
            if report.outcome == 'failed':
                print(f'<LAVA_SIGNAL_STARTTC {test_name}>')
                print('ERROR:\n')
                print(report.longrepr)
                print(f'<LAVA_SIGNAL_ENDTC {test_name}>')
        if done:
            print(f'<LAVA_SIGNAL_TESTCASE TEST_CASE_ID={test_name} RESULT={test_result}>\n')
            # Delay to slow down serial output for LAVA
            time.sleep(0.25)

        # Quiet short result output
        category, short, verbose = '', '', ''
        if hasattr(report, 'wasxfail'):
            if report.skipped:
                category = 'xfailed'
            elif report.passed:
                category = 'xpassed'
            return (category, short, verbose)
        elif report.when in ('setup', 'teardown'):
            if report.failed:
                category = 'error'
            elif report.skipped:
                category = 'skipped'
            return (category, short, verbose)
        category = report.outcome
        return (category, short, verbose)


async def ssh_helper(address, cmd):
    ssh = await asyncssh.connect(address, username='root', known_hosts=None)
    return await ssh.run(cmd)


def pytest_collection_modifyitems(config, items):
    # Check for J1939 support, mark those tests as skip if not present
    have_j1939 = False
    cmd = [ 'grep', '-q', '^CAN_J1939', '/proc/net/protocols' ]
    address = os.environ.get('AGL_TGT_IP', 'localhost')
    if address != 'localhost':
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        result = loop.run_until_complete(ssh_helper(address, ' '.join(cmd)))
        have_j1939 = result.exit_status == 0
    else:
        result = subprocess.run(cmd)
        have_j1939 = result.returncode == 0

    if not have_j1939:
        skip_j1939 = pytest.mark.skip(reason="J1939 protocol support not available")
        for item in items:
            if "can_j1939" in item.keywords:
                item.add_marker(skip_j1939)

    # For now, skip signal-composer tests if not explicitly enabled
    if not config.getoption("--run-signal-composer"):
        skip_signal_composer = pytest.mark.skip(reason="need --run-signal-composer option to run")
        for item in items:
            if "signal_composer" in item.keywords:
                item.add_marker(skip_signal_composer)
