# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from pyagl.services.base import AGLBaseService, AFBResponse
import asyncio
import json


class WeatherService(AGLBaseService):
    service = 'agl-service-weather'
    parser = AGLBaseService.getparser()
    parser.add_argument('--current', default=True, help='Request current weather state', action='store_true')
    parser.add_argument('--apikey', default=False, help='Request weather API Key', action='store_true')

    def __init__(self, ip, port=None):
        super().__init__(api='weather', ip=ip, port=port, service='agl-service-weather')

    async def current_weather(self):
        return await self.request('current_weather', "")

    async def apikey(self):
        return await self.request('api_key', "")


async def main():
    args = WeatherService.parser.parse_args()
    aws = await WeatherService(ip=args.ipaddr, port=args.port)
    if args.current:
        msgid = await aws.current_weather()
        resp = await aws.afbresponse()
        print(json.dumps(resp.data, indent=2))

    if args.apikey:
        msgid = await aws.apikey()
        resp = await aws.afbresponse()
        print(resp.data['api_key'])

    if args.subscribe:
        for event in args.subscribe:
            msgid = await aws.subscribe(event)
            print(f'Subscribed for event {event} with messageid {msgid}')
            resp = await aws.afbresponse()
            print(resp)

    if args.listener:
        async for response in aws.listener():
            print(response)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
