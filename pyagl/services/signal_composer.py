# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from pyagl.services.base import AGLBaseService, AFBResponse
import asyncio
import os


class SignalComposerService(AGLBaseService):
    service = 'service-signal-composer'
    parser = AGLBaseService.getparser()

    def __init__(self, ip, port=None, service='service-signal-composer'):
        super().__init__(api='signal-composer', ip=ip, port=port, service=service)
        # more init stuff specific to the new service

    async def subscribe(self, event):
        return await super().subscribe(event)

    async def unsubscribe(self, event):
        return await super().unsubscribe(event)

    async def addObjects(self, objects):
        return await self.request('addObjects', objects)

    async def get(self, values):
        return await self.request('get', values)

    async def list(self):
        return await self.request('list')


async def main(loop):
    args = SignalComposerService.parser.parse_args()
    svc = await SignalComposerService(args.ipaddr)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
