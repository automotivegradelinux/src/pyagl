# Copyright (C) 2020 Konsulko Group
# Author: Scott Murray
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from pyagl.services.base import AGLBaseService, AFBResponse
from typing import Union
import logging
import asyncio
import os

class AFBRadioResponse(AFBResponse):
    status: str
    info: str
    data = None

    def __init__(self, data: AFBResponse):
        if isinstance(data, list):
            super().__init__(data)
        self.msgid = data.msgid
        self.type = data.type
        self.data = data.data

verbs = ['subscribe', 'unsubscribe', 'frequency', 'band', 'band_supported', 'frequency_range', 'frequency_step', 'start', 'stop', 'scan_start', 'scan_stop', 'stereo_mode', 'rds', 'quality', 'alternative_frequency']
events = ['frequency', 'station_found', 'status', 'rds']

class RadioService(AGLBaseService):
    service = 'agl-service-radio'
    parser = AGLBaseService.getparser()
    parser.add_argument('--start', help='Start radio playback', action='store_true')
    parser.add_argument('--stop', help='Stop radio playback', action='store_true')
    parser.add_argument('--scan-start', help='Start radio scanning(e.g. forward, backward)')
    parser.add_argument('--scan-stop', help='Stop radio scanning', action='store_true')
    parser.add_argument('--frequency', help='get/set tuned radio frequency(e.g. 101100000)')
    parser.add_argument('--band', help='get/set current band type(e.g. AM, FM)')
    parser.add_argument('--band_supported', help='check if a certain band is supported(e.g. AM, FM)')
    parser.add_argument('--frequency_range', help='get frequency range for band type(e.g. AM, FM)')
    parser.add_argument('--frequency_step', help='get frequency step/spacing for band type(e.g. AM, FM)')
    parser.add_argument('--stereo_mode', help='get/set stereo or mono mode(e.g. stereo, mono)')
    # FIXME: Add rest of verb arguments...

    def __await__(self):
        return super()._async_init().__await__()

    def __init__(self, ip, port=None):
        super().__init__(api='radio', ip=ip, port=port, service='agl-service-radio')

    async def frequency(self, value=None):
        msg = None
        if value is not None:
            msg = {'value': value}
        return await self.request('frequency', msg)

    async def band(self, value=None):
        msg = None
        if value is not None:
            msg = {'value': value}
        return await self.request('band', msg)

    async def band_supported(self, band=None):
        msg = None
        if band is not None:
            msg = {'band': band}
        return await self.request('band_supported', msg)

    async def frequency_range(self, band=None):
        msg = None
        if band is not None:
            msg = {'band': band}
        return await self.request('frequency_range', msg)

    async def frequency_step(self, band=None):
        msg = None
        if band is not None:
            msg = {'band': band}
        return await self.request('frequency_step', msg)

    async def start(self):
        return await self.request('start')

    async def scan_start(self, direction=None):
        msg = None
        if direction is not None:
            msg = {'direction': direction}
        return await self.request('scan_start', msg)

    async def stop(self):
        return await self.request('stop')

    async def scan_stop(self):
        return await self.request('scan_stop')

    async def stereo_mode(self, value=None):
        msg = None
        if value is not None:
            msg = {'value': value}
        return await self.request('stereo_mode', msg)

    async def subscribe(self, event=None):
        return await super().subscribe(event=event)

    async def unsubscribe(self, event=None):
        return await super().unsubscribe(event=event)

async def main(loop):
    args = RadioService.parser.parse_args()
    RS = await RadioService(ip=args.ipaddr)

    if args.start:
        msgid = await RS.start()
        r = await RS.afbresponse()
        print(r)

    if args.stop:
        msgid = await RS.stop()
        r = await RS.afbresponse()
        print(r)

    if args.scan_start:
        msgid = await RS.scan_start(args.scan_start)
        r = await RS.afbresponse()
        print(r)

    if args.scan_stop:
        msgid = await RS.scan_stop()
        r = await RS.afbresponse()
        print(r)

    if args.subscribe:
        for event in args.subscribe:
            msgid = await RS.subscribe(event)
            print(f"Subscribed for event {event} with messageid {msgid}")
            r = await RS.afbresponse()
            print(r)

    if args.unsubscribe:
        for event in args.unsubscribe:
            msgid = await RS.unsubscribe(event)
            print(f"Unsubscribed for event {event} with messageid {msgid}")
            r = await RS.afbresponse()
            print(r)

    if args.listener:
        async for response in RS.listener():
            print(response)

    if args.frequency:
        msgid = await RS.frequency(args.frequency)
        r = await RS.afbresponse()
        print(r)

    if args.band:
        msgid = await RS.band(args.band)
        r = await RS.afbresponse()
        print(r)

    if args.band_supported:
        msgid = await RS.band_supported(args.band_supported)
        r = await RS.afbresponse()
        print(r)

    if args.frequency_range:
        msgid = await RS.frequency_range(args.frequency_range)
        r = await RS.afbresponse()
        print(r)

    if args.frequency_step:
        msgid = await RS.frequency_step(args.frequency_step)
        r = await RS.afbresponse()
        print(r)

    if args.stereo_mode:
        msgid = await RS.stereo_mode(args.stereo_mode)
        r = await RS.afbresponse()
        print(r)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
