# Copyright (C) 2021 Fujitsu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from pyagl.services.base import AGLBaseService, AFBResponse
import asyncio
import os

class TelePhonyService(AGLBaseService):
    service = 'agl-service-telephony'
    parser = AGLBaseService.getparser()

    def __init__(self, ip, port=None):
        super().__init__(api='telephony', ip=ip, port=port, service='agl-service-telephony')

    async def subscribe(self, event='callStateChanged'):
        return await super().subscribe(event=event)

    async def unsubscribe(self, event='callStateChanged'):
        return await super().subscribe(event=event)

    async def dial(self, value=None):
        print(value)
        return await self.request('dial', {'value': value})

    async def last_dial(self):
        return await self.request('last_dial')

    async def hangup_all(self):
        return await self.request('hangup_all')

    async def send_tones(self, value: str):
        return await self.request('send_tones', {'value': value})

    async def hangup(self, id: str):
        return await self.request('hangup', {'id': id})

    async def answer(self):
        return await self.request('answer')

    async def hold_and_answer(self):
        return await self.request('hold_and_answer')

    async def release_and_answer(self):
        return await self.request('release_and_answer')

    async def hangup_multiparty(self):
        return await self.request('hangup_multiparty')

    async def create_multiparty(self):
        return await self.request('create_multiparty')

    async def swap_calls(self):
        return await self.request('swap_calls')

    async def get_calls(self):
        return await self.request('get_calls')

    async def get_battery_level(self):
        return await self.request('get_battery_level')

    async def get_network_registration(self):
        return await self.request('get_network_registration')

async def main(loop):
    args = TelePhonyService.parser.parse_args()
    tps = await TelePhonyService(ip=args.ipaddr, port=args.port)

    if args.subscribe:
        for event in args.subscribe:
            msgid = await tps.subscribe(event)
            print(f'Subscribed for event {event} with messageid {msgid}')
            print(await tps.afbresponse())


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
