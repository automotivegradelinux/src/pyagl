# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from pyagl.services.base import AGLBaseService, AFBResponse
import asyncio
import os
import json

class HVACService(AGLBaseService):
    service = 'agl-service-hvac'
    parser = AGLBaseService.getparser()
    parser.add_argument('--temp_left_zone', action='store_true')
    parser.add_argument('--temp_right_zone', action='store_true')
    parser.add_argument('--temp_left_led', action='store_true')
    parser.add_argument('--temp_right_led', action='store_true')
    parser.add_argument('--get_fanspeed', action='store_true')
    parser.add_argument('--get', action='store_true')
    parser.add_argument('--set')  # this accepts JSON formatted string, beware with shell input and quote escaping
    # python3 -m pyagl.services.hvac --set "{\"FanSpeed\": 100}"

    def __init__(self, ip, port=None, service='agl-service-hvac'):
        super().__init__(api='hvac', ip=ip, port=port, service=service)

    async def get_temp_left_zone(self):
        return await self.request('get_temp_left_zone')

    async def get_temp_right_zone(self):
        return await self.request('get_temp_right_zone')

    async def get_fanspeed(self):
        return await self.request('get_fanspeed')

    async def temp_left_zone_led(self):
        return await self.request('temp_left_zone_led')

    async def temp_right_zone_led(self):
        return await self.request('temp_right_zone_led')

    async def get(self, values=None):
        return await self.request('get', values)

    async def set(self, values):
        return await self.request('set', values)


async def main(loop):
    args = HVACService.parser.parse_args()
    hvs = await HVACService(args.ipaddr)

    if args.loglevel:
        hvs.logger.setLevel(args.loglevel)

    if args.temp_left_zone:
        msgid = await hvs.get_temp_left_zone()
        resp = await hvs.afbresponse()
        print(resp)

    if args.temp_right_zone:
        msgid = await hvs.get_temp_right_zone()
        resp = await hvs.afbresponse()
        print(resp)

    if args.get_fanspeed:
        msgid = await hvs.get_fanspeed()
        resp = await hvs.afbresponse()
        print(resp)

    if args.temp_left_led:
        msgid = await hvs.temp_left_zone_led()
        resp = await hvs.afbresponse()
        print(resp)

    if args.temp_right_led:
        msgid = await hvs.temp_right_zone_led()
        resp = await hvs.afbresponse()
        print(resp)

    if args.get:
        msgid = await hvs.get(args.get)
        resp = await hvs.afbresponse()
        print(resp)

    if args.set:
        msgid = await hvs.set(json.loads(args.set))
        resp = await hvs.afbresponse()
        print(resp)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
