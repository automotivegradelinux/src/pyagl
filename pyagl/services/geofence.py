# Copyright (C) 2021 Fujitsu
# Author: Qiu Tingting
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from pyagl.services.base import AGLBaseService, AFBResponse
import asyncio
import os


class GeofenceService(AGLBaseService):
    service = 'agl-service-geofence'
    parser = AGLBaseService.getparser()
    parser.add_argument('--list_fences', action='store_true', help='list current bounding boxes and state.')
    parser.add_argument('--add_fence', help='add geofence bounding box.')
    parser.add_argument('--remove_fence', help='remove named geofence.')
    parser.add_argument('--dwell_transition', help='get/set dwell transition time interval.')

    def __init__(self, ip, port=None):
        super().__init__(api='geofence', ip=ip, port=port, service='agl-service-geofence')

    async def list_fences(self):
        return await self.request('list_fences')

    async def add_fence(self, name=None):
        msg={'name': name, 'bbox': { 'min_latitude': '45.600136', 'max_latitude': '45.600384', 'min_longitude': '-122.499217', 'max_longitude': '-122.498732'}}
        return await self.request('add_fence', msg)

    async def remove_fence(self, name=None):
        return await self.request('remove_fence', {'name': name})

    async def dwell_transition(self, value=None):
        return await self.request('dwell_transition', {'value': value})

    async def subscribe(self, event=None):
        return await self.request('subscribe', {'value': event})

    async def unsubscribe(self, event=None):
        return await self.request('unsubscribe', {'value': event})


async def main(loop):
    args = GeofenceService.parser.parse_args()
    gfs = await GeofenceService(args.ipaddr, args.port)

    if args.list_fences:
        msgid = await gfs.list_fences()
        print(f'Sent list_fences request with value {args.list_fences} and messageid {msgid}')
        resp = await gfs.afbresponse()
        print(resp)

    if args.add_fence:
        msgid = await gfs.add_fence(args.add_fence)
        print(f'Sent add_fence request with value {args.add_fence} and messageid {msgid}')
        resp = await gfs.afbresponse()
        print(resp)

    if args.remove_fence:
        msgid = await gfs.remove_fence(args.remove_fence)
        print(f'Sent remove_fence request with value {args.remove_fence} and messageid {msgid}')
        resp = await gfs.afbresponse()
        print(resp)

    if args.dwell_transition:
        msgid = await gfs.dwell_transition(args.dwell_transition)
        print(f'Sent dwell_transition request with value {args.dwell_transition} and messageid {msgid}')
        resp = await gfs.afbresponse()
        print(resp)


    if args.subscribe:
        for event in args.subscribe:
            msgid = await gfs.subscribe(event)
            print(f'Subscribing for event {event} with messageid {msgid}')
            resp = await gfs.afbresponse()
            print(resp)

    if args.unsubscribe:
        for event in args.unsubscribe:
            msgid = await gfs.unsubscribe(event)
            print(f'Unsubscribing for event {event} with messageid {msgid}')
            resp = await gfs.afbresponse()
            print(resp)


    if args.listener:
        async for response in gfs.listener():
            print(response)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
