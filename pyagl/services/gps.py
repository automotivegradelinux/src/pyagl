# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from pyagl.services.base import AGLBaseService, AFBResponse
import asyncio
import os


class GPSService(AGLBaseService):
    service = 'agl-service-gps'
    parser = AGLBaseService.getparser()
    parser.add_argument('--record', help='Begin recording verb ')
    parser.add_argument('--location', help='Get current location', action='store_true')

    def __init__(self, ip, port=None):
        super().__init__(api='gps', ip=ip, port=port, service='agl-service-gps')

    async def location(self):
        return await self.request('location')

    async def record(self, state='on'):
        return await self.request('record', {'state': state})

    async def subscribe(self, event='location'):
        return await super().subscribe(event=event)

    async def unsubscribe(self, event='location'):
        return await super().subscribe(event=event)


async def main(loop):
    args = GPSService.parser.parse_args()
    gpss = await GPSService(ip=args.ipaddr, port=args.port)

    if args.loglevel:
        gpss.logger.setLevel(args.loglevel)

    if args.record:
        msgid = await gpss.record(args.record)
        print(f'Sent gps record request with value {args.record} with messageid {msgid}')
        print(await gpss.afbresponse())

    if args.location:
        msgid = await gpss.location()
        print(await gpss.afbresponse())

    if args.subscribe:
        for event in args.subscribe:
            msgid = await gpss.subscribe(event)
            print(f'Subscribed for event {event} with messageid {msgid}')
            print(await gpss.afbresponse())

    if args.listener:
        async for response in gpss.listener():
            print(response)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
