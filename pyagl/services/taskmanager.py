# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from pyagl.services.base import AGLBaseService, AFBResponse
import asyncio
import os


class TaskManagerService(AGLBaseService):
    service = 'agl-service-taskmanager'
    parser = AGLBaseService.getparser()

    parser.add_argument('--get_process_list', action='store_true')
    parser.add_argument('--get_netstat', action='store_true')
    parser.add_argument('--get_load_avg', action='store_true')
    parser.add_argument('--get_extra_info', help='unique process ID', type=int)
    parser.add_argument('--kill_process', help='unique process ID', type=int)

    def __init__(self, ip, port=None, service='agl-service-taskmanager'):
        super().__init__(api='taskmanager', ip=ip, port=port, service=service)

    async def get_process_list(self):
        return await self.request('get_process_list')

    async def get_load_avg(self):
        return await self.request('get_load_avg')

    async def get_netstat(self):
        return await self.request('get_netstat')

    async def kill_process(self, tid=None):
        return await self.request('kill_process', tid)

    async def get_extra_info(self, tid=None):
        return await self.request('get_extra_info', tid)

    async def agent_response(self):
        pass

async def main(loop):
    args = TaskManagerService.parser.parse_args()
    tasks = await TaskManagerService(ip=args.ipaddr, port=args.port)

    if args.get_process_list:
        msgid = await tasks.get_process_list()
        print(f'Sent status request with messageid {msgid}')
        resp = await tasks.afbresponse()
        print(resp.data)

    if args.get_netstat:
        msgid = await tasks.get_netstat()
        print(f'Sent status request with messageid {msgid}')
        resp = await tasks.afbresponse()
        print(resp.data)

    if args.get_load_avg:
        msgid = await tasks.get_load_avg()
        print(f'Sent status request with messageid {msgid}')
        resp = await tasks.afbresponse()
        print(resp.data)

    if args.get_extra_info:
        msgid = await tasks.get_extra_info(args.get_extra_info)
        print(f'Sent tid={args.get_extra_info} request with messageid {msgid}')
        resp = await tasks.afbresponse()
        print(resp)

    if args.kill_process:
        msgid = await tasks.kill_process(args.kill_process)
        print(f'Sent tid={args.kill_process} request with messageid {msgid}')
        resp = await tasks.afbresponse()
        print(resp)

    if args.listener:
        for response in nets.listener():
            print(response)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))

