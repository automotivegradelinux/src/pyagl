# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from pyagl.services.base import AGLBaseService, AFBResponse
import asyncio
import os

verbs = ['subscribe', 'unsubscribe', 'media_result']


class MediaScannerService(AGLBaseService):
    service = 'agl-service-mediascanner'
    parser = AGLBaseService.getparser()
    parser.add_argument('--media_result', help='Query media_results verb', action='store_true')

    def __init__(self, ip, port=None, service='agl-service-mediascanner'):
        super().__init__(api='mediascanner', ip=ip, port=port, service=service)
        # more init stuff specific to the new service

    async def media_result(self):
        return await self.request('media_result')

    async def subscribe(self, event='media_added'):
        return await super().subscribe(event)

    async def unsubscribe(self, event='media_added'):
        return await super().unsubscribe(event)


async def main(loop):
    args = MediaScannerService.parser.parse_args()
    svc = await MediaScannerService(args.ipaddr)

    if args.media_result:
        msgid = await svc.media_result()
        print(f'Sent media_result request with msgid {msgid}')
        print(await svc.afbresponse())

    if args.subscribe:
        for event in args.subscribe:
            msgid = await svc.subscribe(event)
            print(f"Subscribed for event {event} with messageid {msgid}")
            r = await svc.afbresponse()
            print(r)

    if args.unsubscribe:
        for event in args.unsubscribe:
            msgid = await svc.unsubscribe(event)
            print(f"Unbscribed from event {event} with messageid {msgid}")
            r = await svc.afbresponse()
            print(r)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
