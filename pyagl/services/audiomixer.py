# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from pyagl.services.base import AGLBaseService, AFBResponse
import asyncio
import os

verbs = ['subscribe', 'unsubscribe', 'list_controls', 'volume', 'mute']
events = ['volume_changed', 'mute_changed', 'controls_changed']


class AudioMixerService(AGLBaseService):
    service = 'agl-service-audiomixer'
    parser = AGLBaseService.getparser()
    parser.add_argument('--list_controls', default=True, help='Request list of controls', action='store_true')
    parser.add_argument('--getmute', help='Get mute state', action='store_true')
    parser.add_argument('--setmute', help='Set mute state', type=int, choices=[0, 1])
    parser.add_argument('--setvolume', help='Set volume level', type=float)
    parser.add_argument('--getvolume', help='Get volume level', action='store_true')

    def __init__(self, ip, port=None, service='agl-service-audiomixer'):
        super().__init__(api='audiomixer', ip=ip, port=port, service=service)

    async def subscribe(self, event=None):  # audio mixer uses 'event' instead 'value',
        return await self.request('subscribe', {'event': event})

    async def unsubscribe(self, event=None):
        return await self.request('unsubscribe', {'event': event})

    async def list_controls(self):
        return await self.request('list_controls')

    async def volume(self, control='Master Playback', value=None):
        if value is not None:
            return await self.request('volume', {'control': control, 'value': value})
        else:
            return await self.request('volume', {'control': control})

    async def mute(self, control='Master Playback', value=None):
        return await self.request('mute', {'control': control, 'value': value})


async def main():
    args = AudioMixerService.parser.parse_args()
    ams = await AudioMixerService(ip=args.ipaddr, port=args.port)

    if args.list_controls:
        resp = await ams.list_controls()
        print(f'Requesting list_controls with id {resp}')
        r = await ams.afbresponse()
        print(r)

    if args.setvolume is not None:
        resp = await ams.volume(value=args.setvolume)
        print(f'Setting volume to {args.setvolume} with id {resp}')
        r = await ams.afbresponse()
        print(r)

    if args.getvolume:
        resp = await ams.volume()
        print(f'Requesting volume with id {resp}')
        r = await ams.afbresponse()
        print(r)

    if args.setmute is not None:
        resp = await ams.mute(args.setmute)
        print(f'Setting mute to {args.setmute} with id {resp}')
        r = await ams.afbresponse()
        print(r)

    if args.getmute:
        resp = await ams.mute()
        r = await ams.afbresponse()
        print(r)

    if args.subscribe:
        for event in args.subscribe:
            msgid = await ams.subscribe(event)
            print(f'Subscribing to {event} with id {msgid}')
            r = await ams.afbresponse()
            print(r)

    if args.unsubscribe:
        for event in args.unsubscribe:
            msgid = await ams.unsubscribe(event)
            print(f'Unsubscribing from {event} with id {msgid}')
            r = await ams.afbresponse()
            print(r)

    if args.listener:
        async for response in ams.listener():
            print(response)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
