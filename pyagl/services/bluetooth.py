# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from pyagl.services.base import AGLBaseService, AFBResponse
import asyncio
import os
import json


Verbs = ['subscribe', 'unsubscribe', 'managed_objects', 'adapter_state', 'default_adapter', 'avrcp_controls',
         'connect', 'disconnect', 'pair', 'cancel_pairing', 'confirm_pairing', 'remove_device']
AdapterStateParams = ['discovery', 'discoverable', 'powered', ]

BTEventType = ['adapter_changes', 'device_changes', 'media', 'agent']


class BluetoothService(AGLBaseService):
    service = 'agl-service-bluetooth'
    parser = AGLBaseService.getparser()
    parser.add_argument('--get_default_adapter', help='Get default bluetooth adapter', action='store_true')
    parser.add_argument('--set_default_adapter', help='Set default bluetooth adapter')
    parser.add_argument('--managed_objects', help='Get managed objects', action='store_true')
    parser.add_argument('--adapter', help='Select remote adapter', required=False, default='hci0')
    parser.add_argument('--adapter_state',)
    parser.add_argument('--connect', help='Connect to device', metavar='dev_88_0F_10_96_D3_20')
    parser.add_argument('--disconnect', help='Disconnect from device', metavar='dev_88_0F_10_96_D3_20')
    parser.add_argument('--pair', help='Pair with a device', metavar='dev_88_0F_10_96_D3_20')
    parser.add_argument('--cancel_pairing', help='Cancel ongoing pairing')
    parser.add_argument('--confirm_pairing',metavar='pincode')
    parser.add_argument('--remove_device', metavar='dev_88_0F_10_96_D3_20', help='Remove paired device')
    parser.add_argument('--avrcp_controls')

    def __init__(self, ip, port=None, service='agl-service-bluetooth'):
        super().__init__(api='Bluetooth-Manager', ip=ip, port=port, service=service)

    async def subscribe(self, event='device_changes'):
        await super().subscribe(event=event)

    async def unsubscribe(self, event='device_changes'):
        await super().unsubscribe(event=event)

    async def managed_objects(self):
        return await self.request('managed_objects')

    async def adapter_state(self, adapter=None, value=None):
        p = {}
        if adapter:
            p = {'adapter': adapter}
            if isinstance(value, dict):
                p = {**p, **value}

        return await self.request('adapter_state', p)

    async def default_adapter(self, adapter=None):
        if adapter is not None:
            return await self.request('default_adapter', {'adapter': adapter})
        else:
            return await self.request('default_adapter')

    async def connect(self, device: str = 'hci0'):
        return await self.request('connect', {'device': device})

    async def disconnect(self, device: str = 'hci0'):
        return await self.request('disconnect', {'device': device})

    async def pair(self, device):
        return await self.request('pair', {'device': device})

    async def cancel_pairing(self):
        return await self.request('cancel_pairing')

    async def confirm_pairing(self, pincode):
        return await self.request('confirm_pairing', {'pincode': pincode})

    async def remove_device(self, device):
        return await self.request('remove_device', {'device': device})

    async def avrcp_controls(self, action):
        return await self.request('avrcp_controls', {'action': action})

async def main(loop):
    args = BluetoothService.parser.parse_args()
    bts = await BluetoothService(ip=args.ipaddr, port=args.port)

    if args.get_default_adapter:
        msgid = await bts.default_adapter()
        print(f'Requesting get default adapter with id {msgid}')
        r = await bts.afbresponse()
        print(r)

    if args.set_default_adapter:
        msgid = await bts.default_adapter(args.set_default_adapter)
        print(f'Requesting set default adapter with id {msgid}')
        r = await bts.afbresponse()
        print(r)

    if args.managed_objects:
        msgid = await bts.managed_objects()
        print(f'Requesting managed objects with id {msgid}')
        r = await bts.afbresponse()
        print(r)

    if args.adapter_state:
        msgid = await bts.adapter_state(args.adapter_state)
        print(f'Requesting adapter state with id {msgid}')
        r = await bts.afbresponse()
        print(r)
    
    if args.connect:
        msgid = await bts.connect(args.connect)
        print(f'Requesting connect with id {msgid}')
        r = await bts.afbresponse()
        print(r)

    if args.disconnect:
        msgid = await bts.disconnect(args.disconnect)
        print(f'Requesting disconnect with id {msgid}')
        r = await bts.afbresponse()
        print(r)

    if args.pair:
        msgid = await bts.pair(args.pair)
        print(f'Requesting pair with id {msgid}')
        r = await bts.afbresponse()
        print(r)

    if args.cancel_pairing:
        msgid = await bts.cancel_pairing()
        print(f'Requesting cancel pairing with id {msgid}')
        r = await bts.afbresponse()

    if args.confirm_pairing:
        msgid = await bts.confirm_pairing(args.confirm_pairing)
        print(f'Requesting confirm pairing with id {msgid}')
        r = await bts.afbresponse()
        print(r)

    if args.remove_device:
        msgid = await bts.remove_device(args.remove_device)
        print(f'Requesting remove device with id {msgid}')
        r = await bts.afbresponse()
        print(r)

    if args.avrcp_controls:
        msgid = await bts.avrcp_controls(args.avrcp_controls)
        print(f'Requesting avrcp controls with id {msgid}')
        r = await bts.afbresponse()
        print(r)


    if args.listener:
        for response in bts.listener():
            print(response)

    bts.logger.debug(await bts.adapter_state('hci0', {'uuids': ['0000110e-0000-1000-8000-00805f9b34fb']}))

    
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
