# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from pyagl.services.base import AGLBaseService, AFBResponse
import asyncio
import os

events = ['tap_shortcut', 'hideWindow', 'replyShowWindow', 'on_screen_message', 'on_screen_reply', 'showNotification',
          'showNotification', 'application-list-changed']


class HomeScreenService(AGLBaseService):
    service = 'agl-service-homescreen'
    parser = AGLBaseService.getparser()
    parser.add_argument('--tap_shortcut', help='Invoke tap_shortcut with parameter')

    def __init__(self, ip, port=None):
        super().__init__(api='homescreen', ip=ip, port=port, service='agl-service-homescreen')

    async def tap_shortcut(self, app: str):
        return await self.request('tap_shortcut', {'application_id': app})

    async def subscribe(self, event):
        return await super().subscribe(event='event')

    async def unsubscribe(self, event):
        return await super().unsubscribe(event='event')

    async def showWindow(self, app: str):
        return await self.request('showWindow', {'application_id': app})

    async def hideWindow(self, app: str):
        return await self.request('hideWindow', {'application_id': app})

    async def ping(self):
        return await self.request('ping')

    async def replyShowWindow(self, app: str):
        return await self.request('replyShowWindow', {'application_id': app})

    async def on_screen_message(self):
        return await self.request('on_screen_message')

    async def on_screen_reply(self):
        return await self.request('on_screen_reply')

    async def getRunnables(self):
        return await self.request('getRunnables')

async def main(loop):
    args = HomeScreenService.parser.parse_args()
    svc = await HomeScreenService(args.ipaddr, args.port)

    if args.loglevel:
        svc.logger.setLevel(args.loglevel)

    if args.tap_shortcut:
        msgid = await svc.tap_shortcut(args.tap_shortcut)
        print(f'Sent tap_shortcut request with value {args.tap_shortcut} and messageid {msgid}')
        print(await svc.afbresponse())

    if args.subscribe:
        for event in args.subscribe:
            msgid = await svc.subscribe(event)
            print(f'Subscribing for event {event} with messageid {msgid}')
            print(await svc.afbresponse())

    if args.listener:
        async for response in svc.listener():
            print(response)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
