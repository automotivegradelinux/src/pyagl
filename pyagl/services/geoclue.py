# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from pyagl.services.base import AGLBaseService, AFBResponse
import asyncio
import os


class GeoClueService(AGLBaseService):
    service = 'agl-service-geoclue'
    parser = AGLBaseService.getparser()
    parser.add_argument('--location', help='Get current location', action='store_true')

    def __init__(self, ip, port=None, api='geoclue'):
        super().__init__(ip=ip, port=port, api=api, service='agl-service-geoclue')

    async def location(self):
        return await self.request('location')

    async def subscribe(self, event='location'):
        return await super().subscribe(event=event)

    async def unsubscribe(self, event='location'):
        return await super().unsubscribe(event=event)


async def main(loop):
    args = GeoClueService.parser.parse_args()
    gcs = await GeoClueService(args.ipaddr)

    if args.location:
        msgid = await gcs.location()
        print(f'Sent location request with messageid {msgid}')
        print(await gcs.afbresponse())

    if args.subscribe:
        for event in args.subscribe:
            msgid = await gcs.subscribe(event)
            print(f"Subscribed for {event} with messageid {msgid}")
            print(await gcs.afbresponse())
    if args.listener:
        async for response in gcs.listener():
            print(response)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
