# Copyright (C) 2021 Fujitsu
# Author: Qiu Tingting
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from pyagl.services.base import AGLBaseService, AFBResponse
import asyncio
import os


class PlatformInfoService(AGLBaseService):
    service = 'agl-service-platform-info'
    parser = AGLBaseService.getparser()
    parser.add_argument('--get', action='store_true', help='Get a platform data.')
    parser.add_argument('--getwithkey', help='Get a platform data with key.')

    def __init__(self, ip, port=None):
        super().__init__(api='platform-info', ip=ip, port=port, service='agl-service-platform-info')

    async def get(self):
        return await self.request('get')

    async def getwithkey(self, key='bb_aglversion'):
        return await self.request('get', key)

    async def subscribe(self, event=None):
        return await self.request('subscribe', {'event': event})

    async def unsubscribe(self, event=None):
        return await self.request('unsubscribe', {'event': event})


async def main(loop):
    args = PlatformInfoService.parser.parse_args()
    svc = await PlatformInfoService(args.ipaddr, args.port)

    if args.get:
        msgid = await svc.get()
        print(f'Sent get request with value {args.get} and messageid {msgid}')
        resp = await svc.afbresponse()
        print(resp)

    if args.getwithkey:
        msgid = await svc.getwithkey(args.getwithkey)
        print(f'Sent get request with value {args.getwithkey} and messageid {msgid}')
        resp = await svc.afbresponse()
        print(resp)

    if args.subscribe:
        for event in args.subscribe:
            msgid = await svc.subscribe(event)
            print(f'Subscribing for event {event} with messageid {msgid}')
            resp = await svc.afbresponse()
            print(resp)

    if args.unsubscribe:
        for event in args.unsubscribe:
            msgid = await svc.unsubscribe(event)
            print(f'Unsubscribing for event {event} with messageid {msgid}')
            resp = await svc.afbresponse()
            print(resp)


    if args.listener:
        async for response in svc.listener():
            print(response)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
