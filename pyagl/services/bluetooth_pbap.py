# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from pyagl.services.base import AGLBaseService, AFBResponse
import json
from pprint import pprint
import asyncio
import os


class BTPBAPService(AGLBaseService):
    service = 'agl-service-bluetooth-pbap'
    parser = AGLBaseService.getparser()

    parser.add_argument('--import_contacts', type=int)
    parser.add_argument('--status', action='store_true')
    parser.add_argument('--contacts', action='store_true')
    parser.add_argument('--search', type=str,help='search telephone number')
    parser.add_argument('--history', choices=['ich','och','mch','cch'], help='Request call history - Incoming/Outgoing'
                                                                             '/Missed/Combined calls')

    def __init__(self, ip, port=None, service='agl-service-bluetooth-pbap'):
        super().__init__(api='bluetooth-pbap', ip=ip, port=port, service=service)

    async def subscribe(self, event='status'):
        return await super().subscribe(event)

    async def unsubscribe(self, event='status'):
        return await super().unsubscribe(event)

    async def import_contacts(self, max_entries):
        return await super().request('import',{'max_entries': max_entries})

    async def status(self):
        return await super().request('status')

    async def contacts(self):
        return await super().request('contacts')

    async def entry(self, handle, param='pb'):
        return await super().request('entry', {'list': param, 'handle': handle})

    async def search(self, number):
        return await super().request('search', {'number': number})

    async def history(self, param):
        return await super().request('history', {'list': param})

async def main(loop):
    args = BTPBAPService.parser.parse_args()
    svc = await BTPBAPService(args.ipaddr, args.port)

    if args.loglevel:
        svc.logger.setLevel(args.loglevel)

    if args.search: 
        msgid= await svc.search(number=args.search)
        print(f'Sent search request with messageid {msgid}')
        print(await svc.afbresponse())

    if args.import_contacts:
        msgid = await svc.import_contacts(max_entries=args.import_contacts)
        print(f'Sent import_contacts request with messageid {msgid}')
        resp = await svc.afbresponse()
        if resp.status == 'success':
            print(json.dumps(resp.data, indent=2, sort_keys=True))
        else:
            print(resp)

    if args.status:
        msgid = await svc.status()
        print(f'Sent status request with messageid {msgid}')
        resp = await svc.afbresponse()
        print(resp.data)

    if args.contacts:
        msgid = await svc.contacts()
        print(f'Sent contacts request with messageid {msgid}')
        print(await svc.afbresponse())

    if args.history:
        msgid = await svc.history(param=args.history)
        print(f'Sent history request with messageid {msgid}')
        print(await svc.afbresponse())

    if args.subscribe:
        for event in args.subscribe:
            msgid = await svc.subscribe(event)
            print(f'Subscribing for event {event} with messageid {msgid}')
            print(await svc.afbresponse())

    if args.listener:
        async for response in svc.listener():
            print(response)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
