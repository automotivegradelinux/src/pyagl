# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from pyagl.services.base import AGLBaseService, AFBResponse
import asyncio
import os


class NetworkService(AGLBaseService):
    service = 'agl-service-network'
    parser = AGLBaseService.getparser()

    parser.add_argument('--state', action='store_true')
    parser.add_argument('--services', action='store_true')

    def __init__(self, ip, port=None, service='agl-service-network'):
        super().__init__(api='network-manager', ip=ip, port=port, service=service)

    async def state(self):
        return await self.request('state')

    async def offline(self, value=True):
        return await self.request('offline', {'value': value})

    async def technologies(self):
        return await self.request('technologies')

    async def get_property(self, technology):
        return await self.request('get_property', {'technology': technology})

    async def set_property(self, technology: str, properties: dict):
        return await self.request('set_property', {'technology': technology, 'properties': properties})

    async def services(self):
        return await self.request('services')

    async def enable_technology(self, technology):
        return await self.request('enable_technology', {'technology': technology})

    async def disable_technology(self, technology):
        return await self.request('disable_technology', {'technology': technology})

    async def scan_services(self, technology):
        return await self.request('scan_services', {'technology': technology})

    async def remove_service(self, service):
        return await self.request('remove_service', {'service': service})

    async def connect_service(self, service):
        return await self.request('connect_service', {'service': service})

    async def disconnect_service(self, service):
        return await self.request('disconnect_service', {'service': service})

    async def agent_response(self):
        pass

async def main(loop):
    args = NetworkService.parser.parse_args()
    nets = await NetworkService(ip=args.ipaddr, port=args.port)

    if args.state:
        msgid = await nets.state()
        print(f'Sent status request with messageid {msgid}')
        resp = await nets.afbresponse()
        print(resp.data)

    if args.services:
        msgid = await nets.services()
        print(f'Sent status request with messageid {msgid}')
        resp = await nets.afbresponse()
        print(resp.data)

    if args.listener:
        for response in nets.listener():
            print(response)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
