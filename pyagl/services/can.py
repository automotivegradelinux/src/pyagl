# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from pyagl.services.base import AGLBaseService, AFBResponse
from typing import Union
import asyncio
import json
import os


class CANService(AGLBaseService):
    service = 'service-can-low'
    parser = AGLBaseService.getparser()
    parser.add_argument('--auth', help='Send an authentication request', action='store_true')
    parser.add_argument('--write', metavar='JSON', help='Write values to CAN')
    parser.add_argument('--list', help='List CAN signals', action='store_true')
    parser.add_argument('--get',type=str, help='get event from CAN')

    def __init__(self, ip, port=None, service='service-can-low-level'):
        super().__init__(api='low-can', ip=ip, port=port, service=service)
        # more init stuff specific to the new service

    async def auth(self):
        return await self.request('auth')

    async def write(self, values: dict, msgid=None):
        return await self.request('write', values, msgid=msgid)

    async def get(self, event):
        return await self.request('get', {'event': event})

    async def list(self):
        return await self.request('list')

    async def subscribe(self, values: Union[str, dict] = ""):
        if isinstance(values, str):
            return await self.request(verb='subscribe', values={'event': values})
        else:
            return await self.request(verb='subscribe', values=values)

    async def unsubscribe(self, values: Union[str, dict] = ""):
        if isinstance(values, str):
            return await self.request(verb='unsubscribe', values={'event': values})
        else:
            return await self.request(verb='unsubscribe', values=values)


async def main(loop):
    args = CANService.parser.parse_args()
    svc = await CANService(args.ipaddr)
    sessionid = 0
    if args.auth:
        sessionid = await svc.auth()
        data = await svc.afbresponse()
        print(data)

    if args.get:
        sessionid = await svc.get(args.get)
        data = await svc.afbresponse()
        print(data)

    if args.list:
        msgid = await svc.list()
        data = await svc.afbresponse()
        print(data)

    if args.write:
        values = json.loads(args.write)
        msgid = await svc.write(values, msgid=sessionid)
        data = await svc.afbresponse()
        print(data)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
