# Copyright (C) 2021 Fujitsu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from pyagl.services.base import AGLBaseService, AFBResponse
import asyncio
import os

class NavigationService(AGLBaseService):
    service = 'agl-service-navigation'
    parser = AGLBaseService.getparser()

    def __init__(self, ip, port=None):
        super().__init__(api='navigation', ip=ip, port=port, service='agl-service-navigation')

    async def subscribe(self, event='status'):
        return await super().subscribe(event=event)

    async def unsubscribe(self, event='status'):
        return await super().unsubscribe(event=event)

    async def broadcast_status(self, state):
        return await self.request('broadcast_status', {'state': state})

    async def broadcast_position(self):
        return await self.request('broadcast_position')

    async def broadcast_waypoints(self):
        return await self.request('broadcast_waypoints')


async def main(loop):
    args = NavigationService.parser.parse_args()
    tps = await NavigationService(ip=args.ipaddr, port=args.port)

    if args.subscribe:
        for event in args.subscribe:
            msgid = await tps.subscribe(event)
            print(f'Subscribed for event {event} with messageid {msgid}')
            print(await tps.afbresponse())

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
