# Copyright (C) 2021 Fujitsu
# Author: liu yahui
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import asyncio
import os
import pytest
import logging
from pyagl.services.base import AFBResponse, AFBT
from pyagl.services.taskmanager import TaskManagerService as TASK

import multiprocessing
import time

def action(max):
    time.sleep(max)

pytestmark = [pytest.mark.asyncio, pytest.mark.taskmanager]

@pytest.fixture(scope='module')
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop


@pytest.fixture(scope='module')
async def service():
    address = os.environ.get('AGL_TGT_IP', 'localhost')
    port = os.environ.get('AGL_TGT_PORT', None)
    tasks = await TASK(ip=address, port=port)
    yield tasks
    await tasks.websocket.close()


async def test_get_process_list(event_loop, service: TASK):
    msgid = await service.get_process_list()
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_get_netstat(event_loop, service: TASK):
    msgid = await service.get_netstat()
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_get_load_avg(event_loop, service: TASK):
    msgid = await service.get_load_avg()
    resp = await service.afbresponse()
    assert resp.status == 'success'

@pytest.mark.xfail(reason='Tests that are known to fail, until the agl-service-taskmanager is fixed in master.')
async def test_get_extra_info(event_loop, service: TASK):
    msgid = await service.get_extra_info(1)
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_kill_process(event_loop, service: TASK):
    mp1 = multiprocessing.Process(target=action,args=(10,))
    mp1.start()
    time.sleep(3)
    print(f'process id: '+str(mp1.pid))
    msgid = await service.kill_process(mp1.pid)
    resp = await service.afbresponse()
    assert resp.status == 'not-replied'
    time.sleep(1)
    assert mp1.is_alive() == False

