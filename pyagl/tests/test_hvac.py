import asyncio
import os
import pytest
import logging
from pyagl.services.base import AFBResponse, AFBT

from pyagl.services.hvac import HVACService as hvs
pytestmark = [pytest.mark.asyncio, pytest.mark.hvac]


@pytest.fixture(scope='module')
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop


@pytest.fixture(scope='module')
async def service():
    address = os.environ.get('AGL_TGT_IP', 'localhost')
    port = os.environ.get('AGL_TGT_PORT', None)
    svc = await hvs(ip=address, port=port)
    yield svc
    await svc.websocket.close()


async def test_get_temp_left_zone(event_loop, service: hvs):
    msgid = await service.get_temp_left_zone()
    resp = await service.afbresponse()
    assert resp.status == 'success'
    assert 'LeftTemperature' in resp.data


async def test_get_temp_right_zone(event_loop, service: hvs):
    msgid = await service.get_temp_right_zone()
    resp = await service.afbresponse()
    assert resp.status == 'success'
    assert 'RightTemperature' in resp.data


async def test_get_fanspeed(event_loop, service: hvs):
    msgid = await service.get_fanspeed()
    resp = await service.afbresponse()
    assert resp.status == 'success'
    assert 'FanSpeed' in resp.data


@pytest.mark.hwrequired
@pytest.mark.xfail(reason='This fails with I2C error due to missing /sys/class/leds/blinkm-3-9-[red,blue,green]')
async def test_temp_left_zone_led(event_loop, service: hvs):
    msgid = await service.temp_left_zone_led()
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


@pytest.mark.hwrequired
@pytest.mark.xfail(reason='This fails with I2C error due to missing /sys/class/leds/blinkm-3-9-[red,blue,green]')
async def test_temp_right_zone_led(event_loop, service: hvs):
    msgid = await service.temp_right_zone_led()
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_get(event_loop, service: hvs):
    msgid = await service.get()
    resp = await service.afbresponse()
    for property in ['FanSpeed', 'LeftTemperature', 'RightTemperature']:
        assert property in resp.data


async def test_set(event_loop, service: hvs):
    msgid = await service.set({'FanSpeed': 15})
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_set_bad_request(event_loop, service: hvs):
    msgid = await service.set({'FanSpeed': -1})
    resp = await service.afbresponse()
    assert resp.status == 'bad-request'

    msgid = await service.set({'FanSpeed': 256})
    resp = await service.afbresponse()
    assert resp.status == 'bad-request'

    msgid = await service.set({'FanSpeed': 'aa'})
    resp = await service.afbresponse()
    assert resp.status == 'bad-request'

async def test_set_success(event_loop, service: hvs):
    msgid = await service.set({'FanSpeed': 255})
    resp = await service.afbresponse()
    assert resp.status == 'success'

    msgid = await service.get_fanspeed()
    resp = await service.afbresponse()
    assert resp.status == 'success'
    assert resp.data == {'FanSpeed': 255}

    msgid = await service.set({'FanSpeed': 0})
    resp = await service.afbresponse()
    assert resp.status == 'success'

    msgid = await service.get_fanspeed()
    resp = await service.afbresponse()
    assert resp.status == 'success'
    assert resp.data == {'FanSpeed': 0}

