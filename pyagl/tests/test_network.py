# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import asyncio
import os
import pytest
import logging
from pyagl.services.base import AFBResponse, AFBT
from pyagl.services.network import NetworkService as NS

pytestmark = [pytest.mark.asyncio, pytest.mark.network]


@pytest.fixture(scope='module')
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop


@pytest.fixture(scope='module')
async def service():
    address = os.environ.get('AGL_TGT_IP', 'localhost')
    port = os.environ.get('AGL_TGT_PORT', None)
    ns = await NS(ip=address, port=port)
    yield ns
    await ns.websocket.close()


@pytest.fixture(scope='module')
def expected_available_interfaces():
    techs = os.environ.get('AGL_AVAILABLE_INTERFACES', 'wifi,ethernet,bluetooth').split(',')
    return techs

@pytest.fixture(scope='module')
def scannable_interfaces():
    scannable = os.environ.get('AGL_SCANNABLE_INTERFACES', 'wifi,bluetooth').split(',')


async def test_state(event_loop, service: NS):
    msgid = await service.state()
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info
#    assert resp.data == 'online'


async def test_global_offline(event_loop, service: NS):
    addr, _ = service.websocket.remote_address
    print(f"Remote address is {addr}")
    if addr != 'localhost' or addr != '127.0.0.1':
        pytest.skip('We want to skip global offline mode until this is ran locally on the board against localhost')
    msgid = await service.offline(True)
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info

async def test_disable_wifi(event_loop, service: NS, expected_available_interfaces):
    if 'wifi' not in expected_available_interfaces:
        pytest.skip('Skipping disable_technology for "wifi" because it is not expected to be available')
    msgid = await service.disable_technology('wifi')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_enable_wifi(event_loop, service: NS, expected_available_interfaces):
    if 'wifi' not in expected_available_interfaces:
        pytest.skip('Skipping enable_technology for "wifi" because it is not expected to be available')
    msgid = await service.enable_technology('wifi')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_enable_bluetooth(event_loop, service: NS, expected_available_interfaces):
    if 'bluetooth' not in expected_available_interfaces:
        pytest.skip('Skipping enable_technology for "bluetooth" because it is not expected to be available')
    msgid = await service.enable_technology('bluetooth')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_disable_bluetooth(event_loop, service: NS, expected_available_interfaces):
    if 'bluetooth' not in expected_available_interfaces:
        pytest.skip('Skipping disable_technology for "bluetooth" because it is not expected to be available')
    msgid = await service.disable_technology('bluetooth')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_enable_ethernet(event_loop, service: NS, expected_available_interfaces):
    if 'ethernet' not in expected_available_interfaces:
        pytest.skip('Skipping enable_technology for "ethernet" because it is not expected to be available')
    msgid = await service.enable_technology('ethernet')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_disable_ethernet(event_loop, service: NS, expected_available_interfaces):
    addr, _ = service.websocket.remote_address
    if addr != 'localhost' or addr != '127.0.0.1':
        pytest.skip('Skipping this test until it is ran locally on the board, '
                    'presuming it is the only available interface to connect to remotely for testing')
    if 'ethernet' not in expected_available_interfaces:
        pytest.skip('Skipping disable_technology for "ethernet" because it is not expected to be available')

    msgid = await service.disable_technology('ethernet')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


@pytest.mark.dependency(depends=['test_enable_wifi'])
async def test_scan_services(event_loop, service: NS, expected_available_interfaces, scannable_interfaces):
    for t in scannable_interfaces:
        if t in expected_available_interfaces:
            msgid = await service.scan_services(technology=t)
            resp = await service.afbresponse()
            assert resp.status == 'success', f'scan_services failed for technology {t} - {resp.info}'


async def test_global_online(event_loop, service: NS):
    msgid = await service.offline(False)
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


@pytest.mark.dependency
async def test_technologies_verb(event_loop, service: NS):
    msgid = await service.technologies()
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info
    assert 'values' in resp.data


@pytest.mark.dependency(depends=['test_technologies_verb'])
async def test_expected_existing_technologies(event_loop, service: NS, expected_available_interfaces):
    msgid = await service.technologies()
    resp = await service.afbresponse()
    techs = [t['technology'] for t in resp.data['values']]
    for t in expected_available_interfaces:
        assert t in techs, f'"{t}" technology is expected to be available, but it is not'


@pytest.mark.dependency(depends=['test_expected_existing_technologies'])
async def test_get_property(event_loop, service: NS, expected_available_interfaces):
    for t in expected_available_interfaces:
        msgid = await service.get_property(t)
        resp = await service.afbresponse()
        assert resp.status == 'success', resp.info
        assert isinstance(resp.data, dict)
        expected_fields = frozenset(['name', 'type', 'powered', 'connected', 'tethering'])
        # diverging_fields = frozenset(expected_fields).symmetric_difference(frozenset(resp.data.keys()))
        diverging_fields = frozenset(expected_fields).difference(frozenset(resp.data.keys()))
        assert len(diverging_fields) == 0, f'the following property fields are diverging from the expected: {diverging_fields}'


@pytest.mark.dependency
@pytest.mark.xfail(reason='Expecting this to throw "permission denied" via the API, tethering from connmanctl succeeds')
async def test_enable_wifi_tethering(event_loop, service: NS, expected_available_interfaces):
    if 'wifi' not in expected_available_interfaces:
        pytest.skip('Skipping wifi tethering test because its not marked as available technology')
    msgid = await service.set_property('wifi', {'tethering': True,
                                                'TetheringIdentifier': 'AGL',
                                                'TetheringPassphrase': 'agltestwifi'})
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


@pytest.mark.dependency(depends='test_enable_wifi_tethering')
async def test_disable_wifi_tethering(event_loop, service: NS, expected_available_interfaces):
    if 'wifi' not in expected_available_interfaces:
        pytest.skip('Skipping wifi tethering test because its not marked as available technology')
    msgid = await service.set_property('wifi', {'tethering': False})
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


#
# async def test_set_property(event_loop, service: NS, expected_available_techs):
#     for t in expected_available_techs:
#         msgid = await service.set_property(t, {'tethering': True})
#         resp = await service.afbresponse()
#         assert resp.status == 'success', resp.info
#         print(resp)


async def test_services_verb(event_loop, service: NS):
    msgid = await service.services()
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info
    assert 'values' in resp.data


async def test_subscribe_global_state(event_loop, service: NS):
    msgid = await service.subscribe('global_state')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_unsubscribe_global_state(event_loop, service: NS):
    msgid = await service.unsubscribe('global_state')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_subscribe_technologies(event_loop, service: NS):
    msgid = await service.subscribe('technologies')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_unsubscribe_technologies(event_loop, service: NS):
    msgid = await service.unsubscribe('technologies')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_subscribe_tech_props(event_loop, service: NS):
    msgid = await service.subscribe('technology_properties')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_unsubscribe_tech_props(event_loop, service: NS):
    msgid = await service.unsubscribe('technology_properties')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_subscribe_services(event_loop, service: NS):
    msgid = await service.subscribe('services')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_unsubscribe_services(event_loop, service: NS):
    msgid = await service.unsubscribe('services')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_subscribe_service_props(event_loop, service: NS):
    msgid = await service.subscribe('service_properties')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_unsubscribe_service_props(event_loop, service: NS):
    msgid = await service.unsubscribe('service_properties')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_subscribe_agent(event_loop, service: NS):
    msgid = await service.subscribe('agent')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_unsubscribe_agent(event_loop, service: NS):
    msgid = await service.unsubscribe('agent')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info



