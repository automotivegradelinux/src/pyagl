
# Copyright (C) 2021 Fujitsu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import asyncio
import os
import pytest
import logging
from pyagl.services.base import AFBResponse, AFBT

from pyagl.services.navigation import NavigationService as ngs
pytestmark = [pytest.mark.asyncio, pytest.mark.navigation]


@pytest.fixture(scope='module')
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop


@pytest.fixture(scope='module')
async def service():
    address = os.environ.get('AGL_TGT_IP', 'localhost')
    port = os.environ.get('AGL_TGT_PORT', None)
    svc = await ngs(ip=address, port=port)
    yield svc
    await svc.websocket.close()


async def test_broadcast_status(event_loop, service: ngs):
    msgid = await service.broadcast_status('stop')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_subscribe(event_loop, service: ngs):
    msgid = await service.subscribe('status')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info
    event = await service.afbresponse()
    assert event.type == AFBT.EVENT
    assert event.api == f'{service.api}/status'


async def test_unsubscribe(event_loop, service: ngs):
    msgid = await service.unsubscribe('status')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_broadcast_position(event_loop, service: ngs):
    msgid = await service.broadcast_position()
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


async def test_broadcast_waypoints(event_loop, service: ngs):
    msgid = await service.broadcast_waypoints()
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info
