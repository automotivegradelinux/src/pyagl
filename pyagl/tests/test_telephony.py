# Copyright (C) 2021 Fujitsu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import asyncio
import os
import pytest
import logging
from pyagl.services.base import AFBResponse
from pyagl.services.telephony import TelePhonyService as tps
pytestmark = [pytest.mark.asyncio, pytest.mark.telephony]


@pytest.fixture(scope='module')
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop


@pytest.fixture(scope='module')
async def service():
    address = os.environ.get('AGL_TGT_IP', 'localhost')
    port = os.environ.get('AGL_TGT_PORT', None)
    svc = await tps(ip=address, port=port)
    yield svc
    await svc.websocket.close()


@pytest.fixture(scope='module')
def phonenumber():
    phonenumenv = os.environ.get('AGL_DIAL_PHONENUM')
    if not phonenumenv:
        pytest.xfail('Please export AGL_DIAL_PHONENUM with valid phone number')

    return phonenumenv


async def test_subscribe(event_loop, service: tps):
    msgid = await service.subscribe('callStateChanged')
    resp = await service.afbresponse()
    assert resp.status == 'success'


async def test_unsubscribe(event_loop, service: tps):
    msgid = await service.unsubscribe('callStateChanged')
    resp = await service.afbresponse()
    assert resp.status == 'success'


@pytest.mark.dependency()
@pytest.mark.hwrequired
async def test_dial(event_loop, service: tps, phonenumber):
    msgid = await service.dial(phonenumber)
    resp = await service.afbresponse()
    assert resp.status == 'success'


@pytest.mark.dependency(depends=['test_dial'])
@pytest.mark.hwrequired
async def test_hangup(event_loop, service: tps):
    await asyncio.sleep(0.5)
    msgid = await service.hangup("voicecall01")
    resp = await service.afbresponse()
    assert resp.status == 'success'


@pytest.mark.dependency(depends=['test_dial'])
@pytest.mark.hwrequired
async def test_hangup_all(event_loop, service: tps):
    msgid = await service.hangup_all()
    resp = await service.afbresponse()
    assert resp.status == 'success'


@pytest.mark.xfail(reason='When someone calls you, run this test to connect')
@pytest.mark.hwrequired
async def test_answer(event_loop, service: tps):
    msgid = await service.answer()
    resp = await service.afbresponse()
    assert resp.status == 'success'


@pytest.mark.hwrequired
async def test_get_calls(event_loop, service: tps):
    msgid = await service.get_calls()
    resp = await service.afbresponse()
    assert resp.status == 'success'


@pytest.mark.hwrequired
async def test_get_battery_level(event_loop, service: tps):
    msgid = await service.get_battery_level()
    resp = await service.afbresponse()
    assert resp.status == 'success'


@pytest.mark.hwrequired
async def test_get_network_registration(event_loop, service: tps):
    msgid = await service.get_network_registration()
    resp = await service.afbresponse()
    assert resp.status == 'success'
