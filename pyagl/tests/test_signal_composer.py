# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import asyncio
import os
import pytest
import logging
from pyagl.services.base import AFBResponse, AFBT
from pyagl.services.signal_composer import SignalComposerService as scs


pytestmark = [pytest.mark.asyncio, pytest.mark.signal_composer]


@pytest.fixture(scope='module')
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop


@pytest.fixture(scope='module')
async def service():
    address = os.environ.get('AGL_TGT_IP', 'localhost')
    port = os.environ.get('AGL_TGT_PORT', None)
    ns = await scs(ip=address, port=port)
    yield ns
    await ns.websocket.close()


@pytest.mark.dependency()
async def test_list(event_loop, service: scs):
    msgid = await service.list()
    resp = await service.afbresponse()
    assert resp is not None, 'list() timed out'
    assert resp.status == 'success'


async def test_getNoFilter(event_loop, service: scs):
    msgid = await service.get({'signal': 'fuel_level'})
    resp = await service.afbresponse()
    assert resp is not None, f'.get() timed out'
    assert resp.status == 'success'


@pytest.mark.dependency(depends=['test_list'])
async def test_getAllSignals(event_loop, service: scs):
    msgid = await service.list()
    resp = await service.afbresponse()
    signals = [s['uid'] for s in resp.data]
    for s in signals:
        msgid = await service.get({'signal':s})
        resp = await service.afbresponse()
        assert resp.status == 'success', f'get() with signal {s} failed, info: {resp.info}'


# while rewriting the old lua tests this one tries to 'get' 'odometer' signal but its not currently present
# async def test_getFilterAvg(event_loop, service: scs):
#     msgid = await service.get({'signal': 'odometer', 'options': {'average': 10}})
#     resp = await service.afbresponse()
#     print(resp)
#     assert resp.status == 'success'


async def test_getFilterMin(event_loop, service: scs):
    msgid = await service.get({'signal': 'latitude', 'options': {'minimum': 10}})
    resp = await service.afbresponse()
    assert resp is not None, '.get() timed out'
    assert resp.status == 'success'


async def test_getFilterMax(event_loop, service: scs):
    msgid = await service.get({'signal': 'vehicle_speed', 'options': {'maximum': 10}})
    resp = await service.afbresponse()
    assert resp is not None, '.get() timed out'
    assert resp.status == 'success'


async def test_subscribe(event_loop, service: scs):
    msgid = await service.subscribe({'service': 'longitude'})
    resp = await service.afbresponse()
    assert resp is not None, ".subscribe() timed out with {'service': 'longitude'}"
    assert resp.status == 'success'


async def test_unsubscribe(event_loop, service: scs):
    msgid = await service.unsubscribe({'service': 'longitude'})
    resp = await service.afbresponse()
    resp is not None, ".unsubscribe() timed out with {'service': 'longitude'}"
    assert resp.status == 'success'
