# Copyright (C) 2021 Fujitsu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import asyncio
import os
import pytest
import logging
from pyagl.services.base import AFBResponse, AFBT

from pyagl.services.mediaplayer import MediaPlayerService as mps
pytestmark = [pytest.mark.asyncio, pytest.mark.mediaplayer]


@pytest.fixture(scope='module')
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop

@pytest.fixture(scope='module')
async def service():
    address = os.environ.get('AGL_TGT_IP', 'localhost')
    port = os.environ.get('AGL_TGT_PORT', None)
    svc = await mps(ip=address, port=port)
    yield svc
    await svc.websocket.close()


async def test_playlist_verb(event_loop,  service: mps):
    msgid = await service.playlist()
    resp = await service.afbresponse()
    assert resp.status == 'success'


async def test_control_play(event_loop, service: mps):
    msgid = await service.control(name='play')
    resp = await service.afbresponse()
    assert resp.status == 'success'


async def test_control_pause(event_loop, service: mps):
    msgid = await service.control(name='pause')
    resp = await service.afbresponse()
    assert resp.status == 'success'


async def test_subscribe_verb(event_loop, service: mps):
    msgid = await service.subscribe('playlist')
    resp = await service.afbresponse()
    assert resp.status == 'success'
    event = await service.afbresponse()
    assert event.type == AFBT.EVENT
    assert event.api == f'{service.api}/playlist'


async def test_unsubscribe_verb(event_loop, service: mps):
     msgid = await service.unsubscribe('playlist')
     resp = await service.afbresponse()
     assert resp.status == 'success'

