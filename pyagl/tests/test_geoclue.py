# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import asyncio
import os
import pytest
import logging
from pyagl.services.base import AFBResponse, AFBT

from pyagl.services.geoclue import GeoClueService as gcs
pytestmark = [pytest.mark.asyncio, pytest.mark.geoclue]


@pytest.fixture(scope='module')
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop


@pytest.fixture(scope='module')
async def service():
    address = os.environ.get('AGL_TGT_IP', 'localhost')
    port = os.environ.get('AGL_TGT_PORT', None)
    svc = await gcs(ip=address, port=port)
    yield svc
    await svc.websocket.close()


@pytest.mark.internet
async def test_location(event_loop, service: gcs):
    msgid = await service.location()
    resp = await service.afbresponse()
    if resp.status == 'failed':
        if resp.info == 'No GeoClue instance available':
            pytest.xfail(resp.info)
    assert resp.msgid == msgid
    assert resp.type == AFBT.RESPONSE
    assert resp.status == 'success', resp.info


@pytest.mark.internet
async def test_subscribe(event_loop, service: gcs):
    msgid = await service.subscribe()
    resp = await service.afbresponse()
    assert resp.status == 'success'
    event = await service.afbresponse()
    assert event.type == AFBT.EVENT  # subscription immediately emits geoclue event
    assert event.api == f'{service.api}/location'


async def test_unsubscribe(event_loop, service: gcs):
    msgid = await service.unsubscribe()
    resp = await service.afbresponse()
    assert resp.status == 'success'
