# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import asyncio
import os
import pytest
import logging
from pyagl.services.base import AFBResponse, AFBT
from pyagl.services.gps import GPSService as GPS
from asyncio.exceptions import TimeoutError

pytestmark = [pytest.mark.asyncio, pytest.mark.gps]


@pytest.fixture(scope='module')
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop


@pytest.fixture(scope='module')
async def service():
    address = os.environ.get('AGL_TGT_IP', 'localhost')
    port = os.environ.get('AGL_TGT_PORT', None)
    gpss = await GPS(ip=address, port=port)
    yield gpss
    await gpss.websocket.close()

# @pytest.fixture(scope='module')
# async def response(event_loop, service):
#     async for _response in service.listener():
#         yield _response


async def test_location_verb(event_loop, service: GPS):
    msgid = await service.location()
    resp = await service.afbresponse()
    assert resp.msgid == msgid


@pytest.mark.xfail(reason='expecting this to fail because of "No 3D GNSS fix" and GPS is unavailable')
async def test_location_result(event_loop, service: GPS):
    msgid = await service.location()
    resp = await service.afbresponse()
    assert resp.status == 'success'


async def test_subscribe_verb(event_loop, service: GPS):
    msgid = await service.subscribe()
    resp = await service.afbresponse()
    assert resp.msgid == msgid
    assert resp.status == 'success'


@pytest.mark.dependency
async def test_enable_recording(event_loop, service: GPS):
    msgid = await service.record()
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info


@pytest.mark.dependency(depends=['test_enable_recording'])
async def test_disable_recording(event_loop, service: GPS):
    msgid = await service.record('off')
    resp = await service.afbresponse()
    assert resp.status == 'success', resp.info



@pytest.mark.dependency
async def test_subscribe_location(event_loop, service: GPS):
    msgid = await service.subscribe('location')
    resp = await service.afbresponse()
    assert resp.msgid == msgid
    assert resp.status == 'success'


@pytest.mark.hwrequired
@pytest.mark.dependency(depends=['test_subscribe_location'])
@pytest.mark.xfail  # expecting this to fail because of "No 3D GNSS fix" and GPS is unavailable
async def test_location_events(event_loop, service: GPS):
    msgid = await service.subscribe('location')
    resp = await service.afbresponse()
    assert resp.msgid == msgid
    assert resp.status == 'success'  # successful subscription
    task = asyncio.create_task(service.afbresponse())
    try:
        resp = await asyncio.wait_for(task, 10)
        assert resp.type == AFBT.EVENT, f'Expected EVENT response, got {resp.type.name} instead'
        # TODO one more assert for the actual received event, haven't received a location event yet
    except TimeoutError:
        task.cancel()
        pytest.xfail("Did not receive location event")


async def test_unsubscribe(event_loop, service: GPS):
    msgid = await service.unsubscribe('location')
    resp = await service.afbresponse()
    assert resp.msgid == msgid
    assert resp.status == 'success'
