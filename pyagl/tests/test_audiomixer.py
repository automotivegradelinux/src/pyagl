# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import asyncio
import os
import pytest

from pyagl.services.base import AFBResponse, AFBT
from pyagl.services.audiomixer import AudioMixerService as AMS
import logging

pytestmark = [pytest.mark.asyncio, pytest.mark.audiomixer]


@pytest.fixture(scope='module')
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop


@pytest.fixture(scope='module')
async def service():
    address = os.environ.get('AGL_TGT_IP', 'localhost')
    port = os.environ.get('AGL_TGT_PORT', None)

    ams = await AMS(ip=address, port=port)
    yield ams
    await ams.websocket.close()


async def test_list_controls(event_loop, service: AMS):
    msgid = await service.list_controls()
    resp = await service.afbresponse()
    assert msgid == resp.msgid
    assert resp.type == AFBT.RESPONSE
    assert resp.status == 'success'


async def test_volume_verb(event_loop, service: AMS):
    msgid = await service.volume()
    resp = await service.afbresponse()
    assert msgid == resp.msgid
    assert resp.type == AFBT.RESPONSE
    assert resp.status == 'success'


async def test_set_volume0(event_loop, service: AMS):
    msgid = await service.volume(value=0)
    resp = await service.afbresponse()
    assert msgid == resp.msgid
    assert resp.type == AFBT.RESPONSE
    assert resp.status == 'success'


async def test_set_maxvolume(event_loop, service: AMS):
    msgid = await service.volume(value=1)
    resp = await service.afbresponse()
    assert msgid == resp.msgid
    assert resp.type == AFBT.RESPONSE
    assert resp.status == 'success'


async def test_get_mute(event_loop, service: AMS):
    msgid = await service.mute()
    resp = await service.afbresponse()
    assert msgid == resp.msgid
    assert resp.type == AFBT.RESPONSE
    assert resp.status == 'success'


async def test_set_mute(event_loop, service: AMS):
    msgid = await service.mute(value=1)
    resp = await service.afbresponse()
    assert msgid == resp.msgid
    assert resp.type == AFBT.RESPONSE
    assert resp.status == 'success'


async def test_set_unmute(event_loop, service: AMS):
    msgid = await service.mute(value=0)
    resp = await service.afbresponse()
    assert msgid == resp.msgid
    assert resp.type == AFBT.RESPONSE
    assert resp.status == 'success'


async def test_subscribe(event_loop,service: AMS):
    msgid = await service.subscribe('volume_changed')
    resp = await service.afbresponse()
    assert msgid == resp.msgid
    assert resp.type == AFBT.RESPONSE
    assert resp.status == 'success'


async def test_unsubscribe(event_loop,service: AMS):
    msgid = await service.unsubscribe('volume_changed')
    resp = await service.afbresponse()
    assert msgid == resp.msgid
    assert resp.type == AFBT.RESPONSE
    assert resp.status == 'success'
