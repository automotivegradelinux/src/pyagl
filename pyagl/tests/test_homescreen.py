# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import asyncio
import os
import pytest
import logging
from pyagl.services.base import AFBResponse, AFBT
from concurrent.futures import TimeoutError

from pyagl.services.homescreen import HomeScreenService as hcs
pytestmark = [pytest.mark.asyncio, pytest.mark.homescreen]


@pytest.fixture(scope='module')
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop


@pytest.fixture(scope='module')
async def service():
    address = os.environ.get('AGL_TGT_IP', 'localhost')
    port = os.environ.get('AGL_TGT_PORT', None)
    svc = await hcs(ip=address, port=port)
    yield svc
    await svc.websocket.close()


async def test_tap_shortcut_media(event_loop, service: hcs):
    msgid = await service.tap_shortcut('mediaplayer')
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_subscribe(event_loop, service: hcs):
    msgid = await service.subscribe(event='tap_shortcut')
    resp = await service.afbresponse()
    assert resp.msgid == msgid

async def test_unsubscribe(event_loop, service: hcs):
    msgid = await service.unsubscribe(event='tap_shortcut')
    resp = await service.afbresponse()
    assert resp.msgid == msgid

async def test_showWindow(event_loop, service: hcs):
    msgid = await service.showWindow('mediaplayer')
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_hideWindow(event_loop, service: hcs):
    misgid = await service.hideWindow('mediaplayer')
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_ping(event_loop, service: hcs):
    misgid = await service.ping()
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_replyShowWindow(event_loop, service: hcs):
    misgid = await service.replyShowWindow('mediaplayer')
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_on_screen_message(event_loop, service: hcs):
    misgid = await service.on_screen_message()
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_on_screen_reply(event_loop, service: hcs):
    misgid = await service.on_screen_reply()
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_getRunnables(event_loop, service: hcs):
    misgid = await service.getRunnables()
    resp = await service.afbresponse()
    assert resp.status == 'success'
