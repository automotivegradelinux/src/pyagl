# Copyright (C) 2021 Fujitsu
# Author: Qiu Tingting
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import asyncio
import os
import pytest
import logging
from pyagl.services.base import AFBResponse, AFBT

from pyagl.services.platforminfo import PlatformInfoService as pis
pytestmark = [pytest.mark.asyncio, pytest.mark.platforminfo]


@pytest.fixture(scope='module')
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop


@pytest.fixture(scope='module')
async def service():
    address = os.environ.get('AGL_TGT_IP', 'localhost')
    port = os.environ.get('AGL_TGT_PORT', None)
    svc = await pis(ip=address, port=port)
    yield svc
    await svc.websocket.close()


async def test_get_all(event_loop, service: pis):
    # test with no para
    msgid = await service.get()
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_get_with_args(event_loop, service: pis):
    # test with para
    msgid = await service.getwithkey('bb_aglversion')
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_subscribe(event_loop, service: pis):
    msgid = await service.subscribe(event='monitor-devices')
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_unsubscribe(event_loop, service: pis):
    msgid = await service.unsubscribe(event='monitor-devices')
    resp = await service.afbresponse()
    assert resp.status == 'success'

