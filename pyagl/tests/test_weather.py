# Copyright (C) 2020 Konsulko Group
# Author: Edi Feschiyan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import asyncio
import os
import pytest
import logging
from pyagl.services.base import AFBResponse, AFBT
from pyagl.services.weather import WeatherService as ws

pytestmark = [pytest.mark.asyncio, pytest.mark.weather]


@pytest.fixture(scope='module')
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop


@pytest.fixture(scope='module')
async def service():
    address = os.environ.get('AGL_TGT_IP', 'localhost')
    port = os.environ.get('AGL_TGT_PORT', None)
    gpss = await ws(ip=address, port=port)
    yield gpss
    await gpss.websocket.close()


async def test_apikey(event_loop, service: ws):
    msgid = await service.apikey()
    resp = await service.afbresponse()
    assert resp.msgid == msgid
    assert resp.data['api_key'] == 'a860fa437924aec3d0360cc749e25f0e'

@pytest.mark.internet
async def test_current_weather(event_loop, service: ws):
    msgid = await service.current_weather()
    resp = await service.afbresponse()
    assert resp.msgid == msgid
    assert 'sys' in resp.data


async def test_bad_subscription(event_loop, service: ws):
    msgid = await service.subscribe('non-existant')
    resp = await service.afbresponse()
    assert resp.status == 'failed'


async def test_bad_unsubscription(event_loop, service: ws):
    msgid = await service.unsubscribe('non-existant')
    resp = await service.afbresponse()
    assert resp.status == 'failed'


@pytest.mark.dependency
@pytest.mark.internet
async def test_subscribe_weather(event_loop, service: ws):
    event = 'weather'
    msgid = await service.subscribe(event)
    resp = await service.afbresponse()
    assert resp.status == 'success'
    eventresp = await service.afbresponse()  # immediately emits event  after subscription
    assert eventresp.api == f'{service.api}/{event}'


@pytest.mark.dependency(depends=['test_subscribe_weather'])
async def test_unsubscribe_weather(event_loop, service: ws):
    msgid = await service.subscribe('weather')
    resp = await service.afbresponse()
    assert resp.status == 'success'
