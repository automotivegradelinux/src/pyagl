# Copyright (C) 2021 Fujitsu
# Author: Qiu Tingting
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import asyncio
import os
import pytest
import logging
from pyagl.services.base import AFBResponse, AFBT

from pyagl.services.geofence import GeofenceService as gfs
pytestmark = [pytest.mark.asyncio, pytest.mark.geofence]


@pytest.fixture(scope='module')
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop


@pytest.fixture(scope='module')
async def service():
    address = os.environ.get('AGL_TGT_IP', 'localhost')
    port = os.environ.get('AGL_TGT_PORT', None)
    svc = await gfs(ip=address, port=port)
    yield svc
    await svc.websocket.close()


async def test_list_fences(event_loop, service: gfs):
    msgid = await service.list_fences()
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_dwell_transition(event_loop, service: gfs):
    msgid = await service.dwell_transition(value='10')
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_add_fence(event_loop, service: gfs):
    msgid = await service.add_fence(name='fence_name')
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_remove_fence(event_loop, service: gfs):
    msgid = await service.remove_fence(name='fence_name')
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_subscribe(event_loop, service: gfs):
    msgid = await service.subscribe(event='fence')
    resp = await service.afbresponse()
    assert resp.status == 'success'

async def test_unsubscribe(event_loop, service: gfs):
    msgid = await service.unsubscribe(event='fence')
    resp = await service.afbresponse()
    assert resp.status == 'success'

